-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.3.16-MariaDB
-- PHP のバージョン: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `todolist`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `account`
--

CREATE TABLE `account` (
  `id_acc` int(11) NOT NULL,
  `username` varchar(65) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(65) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `account`
--

INSERT INTO `account` (`id_acc`, `username`, `password`) VALUES
(1, 'son.latung', '123'),
(2, 'VTI', 'abc123');

-- --------------------------------------------------------

--
-- テーブルの構造 `todotask`
--

CREATE TABLE `todotask` (
  `id` int(11) NOT NULL,
  `username` varchar(65) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(65) NOT NULL,
  `des` text NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `favorite` tinyint(1) NOT NULL,
  `done` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `todotask`
--

INSERT INTO `todotask` (`id`, `username`, `title`, `des`, `start`, `end`, `favorite`, `done`) VALUES
(1, 'VTI', 'Presentation', 'abc', '2019-06-19', '2019-07-08', 0, 1),
(2, 'son.latung', 'Schedule', 'Code todolist', '2019-06-19', '2019-06-23', 0, 1),
(3, 'son.latung', 'todolist', 'Hoan thanh code frondend&backend', '2019-06-20', '2019-06-25', 0, 1),
(4, 'son.latung', 'abc', '123', '2019-07-01', '2019-08-17', 0, 1),
(8, 'VTI', 'Test', 'Test', '2019-06-26', '2019-07-03', 0, 1),
(12, 'VTI', 'Project A', 'ABC123', '2019-06-28', '2019-07-03', 0, 1),
(14, 'VTI', 'ABC123', '12345', '2019-06-28', '0000-00-00', 0, 0),
(20, 'VTI', 'Todo1', '12345', '2019-07-03', '2019-08-04', 0, 0),
(22, 'VTI', 'Todo2', '12345', '2019-07-03', '2019-09-05', 0, 0),
(24, 'VTI', 'Todo3', 'abcde', '2019-07-03', '2019-07-08', 0, 0);

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id_acc`);

--
-- テーブルのインデックス `todotask`
--
ALTER TABLE `todotask`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `account`
--
ALTER TABLE `account`
  MODIFY `id_acc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- テーブルのAUTO_INCREMENT `todotask`
--
ALTER TABLE `todotask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
