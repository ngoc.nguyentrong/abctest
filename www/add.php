<?php
session_start();
if(!isset($_SESSION['LoginUser'])){
	header("location: login.php");
}
?>



<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link href="ngoc.css" rel="stylesheet" type="text/css" media="all">

    <style>
    .btn-xl {
        padding: 0px 40px;
        font-size: 25px;
        border-radius: 0px;
    }

    </style>

</head>
<body>
<div class="container">

<div class="row navi">
<div class="col-3">
    <li><a class="active" href="index.php">Index</a></li>
</div>
    <h4 class="col-6 text-center title1">Add</h4>
</div>



	<br>

	
<form action="crud_process.php" method="POST">
	<div class="row">
		<div class="col-md-6">
			<label for="title">Title:</label>

			<input class="text-border" style="width: 400px;"  type="label" id="title" placeholder="Input title here" name="title" >

		</div>


		<div class="col-md-6">
			<div class="float-right">
			<label for="due">Duedate:</label>
			<input class="text-border " style=" width: 150px;"  type="label" id="due" placeholder="Due Date" name="due" onfocusin="(this.type='date')" onfocusout="(this.type='label')">
			</div>
		</div>

	</div>


	<div class="row">
	<div class="col-md">
		<label>Description</label>
		<textarea class="form-control" id="exampleFormControlTextarea3" rows="7" name="des"></textarea>
	</div>	
	</div>
	<div class="row">
	<div class="col-sm" style="padding: 5px 10px; margin: 0">
		<label>Created date   </label>
		<p class="text-border padtext" style="width: 100px; background-color:white;" id="created" placeholder="<?php echo $a; ?>">
			<?php echo date("Y-m-d"); ?>
		</p>
		<input type="hidden" name="created" value=<?php echo date("Y/m/d"); ?>>

	</div>
	</div>

	<button type="submit" name="submited" value="add_cancel" class="btn btn-outline-danger float-right">Cancel</button>
	<button type="submit" name="submited" value="add_ok" class="btn btn-outline-success float-right">Apply</button>

</form>

</div>

<!-- Load jquery trước khi load bootstrap js -->
<script src="jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
