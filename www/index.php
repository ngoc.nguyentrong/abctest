<?php
include('loginprocess.php');
security();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TodoList</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="script_favo_done.js"></script>
</head>
<body>
    <h1>Todo List</h1>
    <div class = "todolist">
	<p id="welcome">Welcome&nbsp<b><?php echo $_SESSION['LoginUser']; ?></b> !</p>
	<p id="logout"><a href="logout.php">Log out</a></p><br>
    </div><br>

    <p><button type="button" class="btn btn-info btn-add" onclick="add()">Add Task</button>
    <button type="button" class="btn btn-info btn-done" onclick="done()">Check Done Task</button><br><br>
    </p>
    <?php
        require "indexprocess.php"; 
    ?>
<script src="jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>

<script>
		function add()
		{
			window.location.href = "add.php";
		}

        function done()
		{
			window.location.href = "done.php";
		}

</script>
</body>
</html>