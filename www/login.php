<?php
include('loginprocess.php');

if(isset($_SESSION['LoginUser'])){
    header("location: index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+HK&display=swap" rel="stylesheet">
</head>
<body>
    <h1 class="sn"> Sign In </h1>
    <div class="container">
				<div class="d-flex justify-content-center h-100">
					<div class="card">
						<div class="card-header">
                            <div id="form">
                                <form action="" method="post">
                                    <p><label class="label">Username</label>
                                        <input type="text" class="form-control" autocomplete="off" required name="username" placeholder="Your Username"/>
                                    </p>
                                    <p><label class="label">Password</label>
                                        <input type="password" class="form-control" required name="password" placeholder="Your Password" /><br>
                                    </p>
                                    <p>
                                        <input type="submit" class="btn float-right btn-success" name="submit" value=" Login "/>
                                    </p>
                                    <span class="errormsg"><?php echo $error; ?></span>
                                </form>
                            </div>
						</div>
					</div>
				</div>
			</div>
</body>
</html>
