<?php
require_once('db_connection.php');  

class customdb
{
    protected $conn;

    function OpenConnect()
    {
        // $servername = "db";
        // $username = "root";
        // $password = "";
        // $db = "todolist";

        // $this->conn = new mysqli($servername, $username, $password, $db);
        // if ($this->conn->connect_error) {
        //     die("Connection failed: " . $this->conn->connect_error);
        // } 
        $this->conn = OpenCon();
    }
    

    function CloseCon()
    {
        $this->conn -> close();
    }

    function read_test()
    {
        $sql = "SELECT id, username, title, des, start, end, favorite, done FROM todotask";
        $result = $this->conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                echo "id: " . $row["id"]. " - " . $row["username"]. " - " . $row["title"]. " - " . $row["des"]. " - " . $row["start"] . " - " . $row["end"]. " - " . $row["favorite"]. " - " . $row["done"];
            }
        } else {
            echo "0 results";
        }
        
        // return $result;
    }


    function add_db($username, $title, $des, $start, $end, $favorite, $done)
    {   
        $sql = "INSERT INTO `todotask` (`id`, `username`, `title`, `des`, `start`, `end`, `favorite`, `done`) VALUES
        (null, '$username', '$title', '$des', '$start', '$end', $favorite, $done);";
        
        if ($this->conn->query($sql) === TRUE) {
            echo "New record was created successfully";
            echo "<br><a class='active' href='index.php'>Back to Index</a>";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    function update_db($id, $username, $title, $des, $end)
    {
        $id = (int) $id;
        $sql = "UPDATE todotask
        SET username = '$username', title = '$title', des = '$des', end = '$end'
        WHERE id=$id;";
        
        if ($this->conn->query($sql) === TRUE) {
            echo " Record was updated successfully";
            echo "<br><a class='active' href='index.php'>Back to Index</a>";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    function update_favo($id)
    {
        $id = (int) $id;
        $sql = "UPDATE todotask SET favorite = 1 - favorite WHERE id=$id;"; 

        if ($this->conn->query($sql) === TRUE) {
            echo " set favorite -- successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    function update_done($id)
    {
        $id = (int) $id;
        $sql = "UPDATE todotask SET done = 1 - done WHERE id=$id;"; 

        if ($this->conn->query($sql) === TRUE) {
            echo " set done -- successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    function read_id($id)
    {
        $data =  array(
            "username" => "",
            "title" => "",
            "des" => "",
            "start" => "",
            "end" => "",
            "favorite" => "",
            "done" => "",
        );

        $sql = "SELECT username, title, des, start, end, favorite, done FROM todotask  WHERE id = $id;";
        $result = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            if ($row = $result->fetch_assoc()) {
                foreach ($row as $key => $value){
                    $data[$key] = $value;
                }
            }
            return $data;
        } else {
            echo "0 results";
            return $data;
        }
    }
}

// $tododb = new customdb;
// $tododb->OpenConnect();

// // $tododb->add_db('tao','Titleso1','1caidesriptionnaodo','2019-06-28','2019-06-30', 0, 1);
// // $tododb->delete(3);
// // $tododb->update_db(3, 'tao','Titleso1','1caidesriptionnaodo','2019-06-28','2019-06-30');

// $tododb->CloseCon();

