<?php
session_start();
if(!isset($_SESSION['LoginUser'])){
	header("location: login.php");
}
$id= $_GET['id'];
include_once('dbconn.php');
$dbreader=new customdb;
$dbreader->OpenConnect();
$data=$dbreader->read_id($id);
$dbreader->CloseCon();
?>



<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link href="ngoc.css" rel="stylesheet" type="text/css" media="all">

    <style>
    .btn-xl {
        padding: 0px 40px;
        font-size: 25px;
        border-radius: 0px;
    }

    </style>

</head>
<body>
<div class="container">

<div class="row navi">
<div class="col-3">
    <li><a class="active" href="index.php">Index</a></li>
</div>
    <h4 class="col-6 text-center title1">Detail</h4>
</div>



	<br>

	

	<div class="row">
		<div class="col-md-6">
			<label for="title">Title:</label>
			<p class="text-border padtext" id="title"  name="title" style="background-color:white;" >
			<?php echo $data["title"]; ?>
			</p>
		</div>


		<div class="col-md-6">
			<div class="float-right">
			<label for="due">Duedate:</label>
			<p class="text-border padtext" id="title" style="width: 100px; background-color:white;"  name="due" >
			<?php echo $data["end"]; ?>
			</p>
			</div>
		</div>

	</div>


	<div class="row">
	<div class="col-md">
		<label>Description</label>
		<p class="text-border padtext" id="title" style="background-color:white; width: 100%; border: 1px double rgba(15, 31, 104, 0.43);  height: 300px;" placeholder="<?php echo $a; ?>" name="des" >
			<?php echo $data["des"]; ?>
		</p>
	</div>	
	</div>
	<div class="row">
	<div class="col-sm" style="padding: 5px 10px; margin: 0">
		<label>Created date   </label>
		<p class="text-border padtext" id="title" style="width: 100px; background-color:white;" name="createdate" >
			<?php echo $data["start"]; ?>
		</p>
	</div>
	</div>




</div>

<!-- Load jquery trước khi load bootstrap js -->
<script src="jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
