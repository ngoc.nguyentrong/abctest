<?php
$conn = OpenCon();

$username = $_SESSION['LoginUser'];
$sql= "SELECT * FROM todotask where username='".$username."' AND done=0 ORDER BY id DESC";
$stmt= mysqli_query($conn,$sql);
if(mysqli_num_rows($stmt) == 0){
   echo "<h2>Blank</h2>";
}
else{ ?>
        <table class="tableindex">
            <tbody>
            <tr>
                <th>Title</th>
                <th>Remaining</th>
                <th>Action</th>
            </tr>
            <?php while($row=mysqli_fetch_array($stmt, MYSQLI_BOTH)){ ?>
              <tr id="todolist-row-<?php echo $row["id"]; ?>">
                <td class="tdindex">
                    <a href="detail.php?id=<?php echo $row["id"];?>"><?php echo $row["title"]; ?></a>           
                </td>
                <td>
                    <?php
                    //Tính thời gian từ deadline cho đến hiện tại
                    $now = date("Y-m-d");
                    $first_date = strtotime($row["end"]);
                    $second_date = strtotime($now);
                    $datediff = abs($first_date - $second_date);
                    $remaining = floor($datediff / (60*60*24));
                    if(($first_date - $second_date)<0){
                        $remaining = 0;
                    }
                    if($remaining>1) //Ham dieu kien thay doi chu day hien thi ra
                    {
                        $day= " days";
                    }
                    else
                    {
                        $day= " day";
                    }                  
                    ?>
                    <?php 
                    if($remaining<8) //Thay doi mau chu khi deadline con 7 ngay tro xuong
                    { ?>
                    <div class="rm"> <?php echo $remaining.$day ?> </div>
                        
                    <?php 
                    }  else{ ?>
                    <div class="rm1"> <?php echo $remaining.$day ?> </div>
                    <?php
                    }                
                    ?>    
                </td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-primary"><a class="linkbtn" href="edit.php?id=<?php echo $row["id"];?>">Edit</a></button>
                        <button type="button" class="btn btn-danger"><a class="linkbtn" href="delete.php?id=<?php echo $row["id"];?>" onClick="return confirm('Are you sure to delete this record?')" >Delete</a></button>
                        <button type="button" class="fa btn btn-default" style="background:<?php echo ($row["favorite"] == "0" ? "white" : "gold"); ?>" onclick="favo_toggle(this)"  value="<?php echo $row["id"]; ?>">Favorite</button>
                        <button type="button" class="btn btn-secondary" onclick="done_toggle(this)" value="<?php echo $row["id"]; ?>" >Done</button>
                    </div>
                </td>			
            </tr>
            <?php } echo "" ?>
            </tbody> 
        </table><br>
      <?php     
}
CloseCon($conn);
?>
<script>
function favo_toggle(x) {

    function myFunction(x) {
      if (x.style.background === "gold") {
        x.style.background = "white";
      } else {
        x.style.background = "gold";
      }
    }

    myFunction(x);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    ;
    }
  };

    xhttp.open("GET", "crud_process.php?favodone=favo&id=" + x.value, true);
    xhttp.send();
}
function done_toggle(x) {
    document.getElementById("todolist-row-" + x.value).style.display='none';

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    ;
    }
  };
    xhttp.open("GET", "crud_process.php?favodone=done&id=" + x.value, true);
    xhttp.send();
}

</script>