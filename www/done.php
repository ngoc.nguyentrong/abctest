<?php
include('loginprocess.php');
$conn = OpenCon();

$username = $_SESSION['LoginUser'];
if(!isset($_SESSION['LoginUser'])){
	header("location: login.php");
}

?>

<script>

function undo(x) {
    document.getElementById("rowid_" + x.value).style.display='none';

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    ;
    }
  };
    xhttp.open("GET", "crud_process.php?favodone=undo&id=" + x.value, true);
    xhttp.send();
}

</script>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Done Task Page</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link href="ngoc.css" rel="stylesheet" type="text/css" media="all">
</head>

<div class="container">

<div class="row navi">
<div class="col-3">
    <li><a class="active" href="index.php">Index</a></li>
</div>
    <h4 class="col-6 text-center title1">Done Task</h4>
</div>

  <br>
  
<table class="tabledone">
	<thead>
		<tr>
			<th>Title</th>
      <th>Created Date</th>
      <th>Deadline</th>
			<th style="width: 60px;">Reopen</th>
		</tr>
	</thead>

	<tbody>
		<?php 
		// select all tasks if page is visited or refreshed
		$sql= "SELECT * FROM todotask where username='".$username."'  AND done=1; ";
    $stmt= mysqli_query($conn,$sql);
    if(mysqli_num_rows($stmt) == 0){
      echo "<h2>Blank</h2>";
    }
    else
    {
		$i = 1; while ($row = mysqli_fetch_array($stmt)) { ?>
        <tr id="rowid_<?php echo $row["id"]; ?>">
        <td class="tdindex"> <?php echo $row['title']; ?> </td>
        <td> <?php echo $row['start']; ?> </td>
        <td> <?php echo $row['end']; ?> </td>
				<td> 
        <button type="button" class="" style="color:red" onclick="undo(this)"  value="<?php echo $row["id"]; ?>">	&#x21BA;</button>
				</td>
			</tr>
		<?php $i++; }} ?>	
	</tbody>
</table>